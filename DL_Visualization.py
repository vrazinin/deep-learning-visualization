from keras.models import Sequential
from keras.layers import Dense
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import keras
import networkx as nx
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import numpy
import random
import pylab
from matplotlib.pyplot import pause
pylab.ion()
from random import randint
import glob
import os

# Fix random seed for reproducibility
seed = 7
numpy.random.seed(seed)

# Loading the dataset
dataset = numpy.loadtxt("pima-indians-diabetes.csv", delimiter=",")
X = dataset[:,0:8]
Y = dataset[:,8]

# *** Set the number of layers ***

# try:
#     N_Layers = int(raw_input('Number of layers:'))
# except ValueError:
#     print "Not a number"

# *** Create a one-dimensional array N_Neurons, the dimension of which is the number of layers,
# and the values - the number of neurons in each layer ***

# N_Neurons = numpy.ones(N_Layers)

# *** Depending on the number of layers, in the cycle assign to each layer (array cell - N_Neurons[i])
# the number of neurons via int(raw_input('Number of neurons in i-layer:')) ***

# for i in range(N_Layers):
    # N_Neurons[i] = int(raw_input('Number of neurons in ' + str(i+1) + '-layer:'))

import random
import string

N_Layers = 6 # number of layers
N_Neurons = numpy.array([8,12,8,12,8,1]) # number of neurons in each layer
N_Neurons_STR = [str(elem) for elem in N_Neurons]
STR_Neurons = ','.join(N_Neurons_STR)

from time import gmtime, strftime
from datetime import datetime

# Creating new directory
path = r'/.../Project/Results_DIR/RESULT_L_' + str(N_Layers) + '_N_(' + STR_Neurons + ')' + '_TIME_(' + strftime("%Y.%m.%d_%H.%M.%S", gmtime()) + ')'
if not os.path.exists(path):
    os.makedirs(path)

columns = 1
for i in range(N_Layers):
    columns *= int(N_Neurons[i])

model = Sequential()
model.add(Dense(N_Neurons[1], input_dim=N_Neurons[0], init='uniform', activation='relu'))

for i in range(1, N_Layers-2):
    model.add(Dense(N_Neurons[i+1], init='uniform', activation='relu'))

model.add(Dense(N_Neurons[N_Layers-1], init='uniform', activation='sigmoid'))

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

epochs = 50 # number of epochs
G = nx.Graph() # creating new graph

centre = max(N_Neurons)/2
p_start = numpy.zeros(N_Layers)
for i in range(N_Layers-1):
    p_start[i] = centre - N_Neurons[i]/2
p_start[N_Layers-1] = centre

for i in range(N_Layers):
    step = 1
    for j in range(int(N_Neurons[i])):
        p = p_start[i] + step
        G.add_node(str(i+1) + '.'+str(j+1),pos=(i+1,p))
        step += 1

pos = nx.get_node_attributes(G,'pos')
colors = range(20)
nodes = nx.draw_networkx_nodes(G,pos,node_size=200,node_color='#3b5998',linewidths=1)
nodes.set_edgecolor('0.1')
nx.draw_networkx_labels(G,pos,font_size=6,font_family='sans-serif',font_color='0.1',)

for i in range(N_Layers-1):
    for ii in range(int(N_Neurons[i])):
        for jj in range(int(N_Neurons[i+1])):
            G.add_edges_from([(str(i+1)+'.'+str(ii+1),str(i+2)+'.'+str(jj+1))])

plt.axis('off')

w = numpy.zeros((epochs,int(columns)))

filepath="weights.best.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
callbacks_list = [checkpoint]

res_acc = numpy.zeros(epochs)

for e in range(epochs):
    print '# ' + str(e)
    model.fit(X, Y, validation_split=0.33, nb_epoch=1, batch_size=10, callbacks=callbacks_list, verbose=0)

    scores = model.evaluate(X, Y, verbose=0)
    res_acc[e] = scores[1]*100

    k = 0
    for i in range(N_Layers-1):
        weights = model.layers[i].get_weights()
        for ii in range(int(N_Neurons[i])):
            for jj in range(int(N_Neurons[i+1])):
                w[e][jj+k] = abs(weights[0][ii][jj])
            k = k + N_Neurons[i+1]

for e in range(epochs):

    plt.axis('off')

    e1 = []
    e2 = []
    e3 = []
    e4 = []
    e5 = []

    n1 = []
    n2 = []
    n3 = []
    n4 = []
    n5 = []

    k = 0
    for i in range(N_Layers-1):
        for ii in range(int(N_Neurons[i])):
            for jj in range(int(N_Neurons[i+1])):
                if 0 <= abs(w[e][jj+k]) <= 0.2:
                    e1.append((str(i+1)+'.'+str(ii+1),str(i+2)+'.'+str(jj+1)))
                    n1.append((str(i+1)+'.'+str(ii+1)))
                    n1.append((str(i+2)+'.'+str(jj+1)))
                elif 0.2 <= abs(w[e][jj+k]) <= 0.4:
                    e2.append((str(i+1)+'.'+str(ii+1),str(i+2)+'.'+str(jj+1)))
                    n2.append((str(i+1)+'.'+str(ii+1)))
                    n2.append((str(i+2)+'.'+str(jj+1)))
                elif 0.4 <= abs(w[e][jj+k]) <= 0.6:
                    e3.append((str(i+1)+'.'+str(ii+1),str(i+2)+'.'+str(jj+1)))
                    n3.append((str(i+1)+'.'+str(ii+1)))
                    n3.append((str(i+2)+'.'+str(jj+1)))
                elif 0.6 <= abs(w[e][jj+k]) <= 0.8:
                    e4.append((str(i+1)+'.'+str(ii+1),str(i+2)+'.'+str(jj+1)))
                    n4.append((str(i+1)+'.'+str(ii+1)))
                    n4.append((str(i+2)+'.'+str(jj+1)))
                elif 0.8 <= abs(w[e][jj+k]) <= 1:
                    e5.append((str(i+1)+'.'+str(ii+1),str(i+2)+'.'+str(jj+1)))
                    n5.append((str(i+1)+'.'+str(ii+1)))
                    n5.append((str(i+2)+'.'+str(jj+1)))
            k = k + N_Neurons[i+1]

    nodes_n1 = nx.draw_networkx_nodes(G,pos,nodelist=n1,node_size=200,node_color='#f7f7f7',linewidths=1)
    nodes_n2 = nx.draw_networkx_nodes(G,pos,nodelist=n2,node_size=200,node_color='#dfe3ee',linewidths=1)
    nodes_n3 = nx.draw_networkx_nodes(G,pos,nodelist=n3,node_size=200,node_color='#8b9dc3',linewidths=1)
    nodes_n4 = nx.draw_networkx_nodes(G,pos,nodelist=n4,node_size=200,node_color='#3b5998',linewidths=1)
    nodes_n5 = nx.draw_networkx_nodes(G,pos,nodelist=n5,node_size=200,node_color='#3b5998',linewidths=1)

    nx.draw_networkx_edges(G,pos,edgelist=e1,width=1,alpha=1,edge_color='0.8')
    nx.draw_networkx_edges(G,pos,edgelist=e2,width=1,alpha=1,edge_color='0.6')
    nx.draw_networkx_edges(G,pos,edgelist=e3,width=1,alpha=1,edge_color='0.4')
    nx.draw_networkx_edges(G,pos,edgelist=e4,width=1,alpha=1,edge_color='0.2')
    nx.draw_networkx_edges(G,pos,edgelist=e5,width=1,alpha=1,edge_color='0.0')
    nx.draw_networkx_labels(G,pos,font_size=6,font_family='sans-serif',font_color='0.1',)

    plt.title("Iteration # " + str(e) + ", Accuracy =  %.2f%%" % (res_acc[e]))
    plt.savefig(path + "/Graph_" + str(e) + ".png", format="PNG", dpi=300)

    plt.clf()

# Creating new gif

gif_name = '/.../Project/Results_GIF/Graph_FIN_L_' + str(N_Layers) + '_N_' + STR_Neurons + '_TIME_' + strftime("%Y.%m.%d_%H.%M.%S", gmtime())

file_list = glob.glob(os.path.join(path, '*.png'))
list.sort(file_list, key=lambda x: int((os.path.basename(x)).split('_')[1].split('.png')[0])) # Sort the images by '_<number>'

with open('image_list.txt', 'w') as file:
    for item in file_list:
        file.write("%s\n" % item)

os.system('convert @image_list.txt {}.gif'.format(gif_name))
